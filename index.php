<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/25/18
 * Time: 2:28 PM
 */

require dirname(__FILE__) . "/vendor/autoload.php";

use Team\Header\HeaderRenderer;
use Team\Banner\BannerRenderer;
use Team\Member\MemberRenderer;

echo (new HeaderRenderer)->render();
echo (new BannerRenderer)->render();
echo (new MemberRenderer)->render();
