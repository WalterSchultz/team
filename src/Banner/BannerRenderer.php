<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/25/18
 * Time: 2:48 PM
 */

namespace Team\Banner;

use Mustache_Engine;
use Mustache_Loader_FilesystemLoader;
use Team\Renderer;

class BannerRenderer implements Renderer
{
    private $message = "Hi!";

    function render(): string
    {
        $mustache = new Mustache_Engine(array(
            'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__))
        ));
        $template = $mustache->loadTemplate('banner');
        return $template->render(array('message' => 'Hello everybody!!!'));
    }
}