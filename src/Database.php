<?php
/**
 * Created by PhpStorm.
 * User: cboyd19
 * Date: 7/26/18
 * Time: 11:52 AM
 */

namespace Team;

// Database Config
use mysqli;

define('DB_HOST', getenv('MYSQL_SERVICE_HOST').':'.getenv('MYSQL_SERVICE_PORT'));
define('DB_USERNAME', 'user4UP');
define('DB_PASSWORD', 'qPUHIgshSjQ6BHCQ');
define('DB_NAME', 'team');

class Database extends mysqli
{
    private static $instance = null;
    private $host = DB_HOST;
    private $username = DB_USERNAME;
    private $password = DB_PASSWORD;
    private $name = DB_NAME;
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    public function __construct()
    {
        parent::__construct($this->host, $this->username, $this->password, $this->name);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
        }
        parent::set_charset('utf-8');
    }
    public function runQuery($query)
    {
        if ($query == "" || !$this->query($query)) {
            return false;
        }
        return true;
    }
    public function getResults($query)
    {
        if (!$this->runQuery($query)) {
            return null;
        }
        $results = array();
        $temp = $this->query($query);
        if ($temp->num_rows > 0) {
            while ($row = $temp->fetch_assoc()) {
                array_push($results, $row);
            }
        }
        return $results;
    }
}