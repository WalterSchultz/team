<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/25/18
 * Time: 3:32 PM
 */

namespace Team\Member;

class Member
{
    public $name = "Anonymous";
    public $image_url = "https://hostadvice.com/wp-content/themes/bestwh/img/avatars/v2/normal.png";

    public function __construct(string $name)
    {
        $this -> name = $name;
    }
}