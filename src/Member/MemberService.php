<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/25/18
 * Time: 3:35 PM
 */
declare(strict_types=1);

namespace Team\Member;

use Team\Database;

final class MemberService
{
    private $members;
    private $my_file = 'members.dat';

    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new MemberService();
        }
        return $inst;
    }

    private function __construct()
    {
//        $this -> members =
    }

    public function getMembers() {
        return Database::getInstance() -> getResults("select * from team.member");
    }

    public function deleteMember($name) {
        echo Database::getInstance() -> runQuery("delete from team.member where member.name = '" . $name . "'");
    }

    protected function __clone()
    {
        //Me not like clones! Me smash clones!
    }

    private function generateUsers() {
        return array(
            new Member("Walter"),
            new Member("Chris"),
            new Member("Jeff"),
            new Member("Brian"),
            new Member("Ron"),
            new Member("Paul")
        );
    }
}