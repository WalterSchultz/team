<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/25/18
 * Time: 2:50 PM
 */

namespace Team\Member;

use Mustache_Engine;
use Mustache_Loader_FilesystemLoader;
use Team\Renderer;

class MemberRenderer implements Renderer
{
    function render(): string
    {
        $mustache = new Mustache_Engine(array(
            'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__))
        ));
        $template = $mustache->loadTemplate('member');
        return $template->render(array('members' => MemberService::Instance() -> getMembers()));
    }
}