<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/25/18
 * Time: 2:48 PM
 */

namespace Team;

interface Renderer
{
    function render(): string;

}