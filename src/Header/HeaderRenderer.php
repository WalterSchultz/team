<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/26/18
 * Time: 8:43 AM
 */

namespace Team\Header;

use Mustache_Engine;
use Mustache_Loader_FilesystemLoader;

class HeaderRenderer
{
    function render(): string
    {
        $mustache = new Mustache_Engine(array(
            'loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__))
        ));
        $template = $mustache->loadTemplate('header');
        return $template->render();
    }
}