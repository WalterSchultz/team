<?php
/**
 * Created by PhpStorm.
 * User: wschultz4
 * Date: 7/26/18
 * Time: 11:06 AM
 */
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Team\Member\Member;
use Team\Member\MemberService;


final class MemberServiceTest extends TestCase
{
    public function testDeleteMember_deletesRequestedMember(): void {
        $instance = MemberService::Instance();
        $vanishingUser = "Walter";

        $memberNames = array_map(function (Member $member):string { return $member -> name; }, $instance -> getMembers());
        $this -> assertContains($vanishingUser, $memberNames);

        $instance -> deleteMember($vanishingUser);

        $memberNames = array_map(function (Member $member):string { return $member -> name; }, $instance -> getMembers());
        $this -> assertNotContains($vanishingUser, $memberNames);
    }

    public function testDeleteMember_onlyDeletesOneMember(): void {
        $instance = MemberService::Instance();
        $vanishingUser = "Walter";

        $initialMemberCount = count($instance -> getMembers());

        $instance -> deleteMember($vanishingUser);

        $this -> assertEquals($initialMemberCount - 1, count($instance -> getMembers()));
    }
}